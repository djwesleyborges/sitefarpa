from django.conf.urls import url
from farpa.views import index

urlpatterns = [
    url(r'^', index, name='index'),
]
